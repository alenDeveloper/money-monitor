package com.example.alen.moneymonitor.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alen.moneymonitor.R;
import com.example.alen.moneymonitor.models.MoneyInputObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {

    List<MoneyInputObject> moneyInputObjects = new ArrayList<>();
    OnMoneyObjectLongClick clickListener;

    public MainRecyclerAdapter(List<MoneyInputObject> list, OnMoneyObjectLongClick clickListener) {
        this.moneyInputObjects = list;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_list_item, parent, false);
        return new ViewHolder(view, clickListener);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MoneyInputObject object = moneyInputObjects.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("dd HH:mm");

        holder.title.setText(object.getTitle());
        holder.date.setText(sdf.format(object.getInputDate()) + " h");
        holder.amount.setText(String.valueOf(object.getAmount()) + " $");

        if (object.isAdding()) {
            holder.amount.setTextColor(Color.GREEN);
        } else {
            holder.amount.setTextColor(Color.RED);
        }
        holder.description = object.getDescription();
        holder.object = object;
    }

    @Override
    public int getItemCount() {
        return moneyInputObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView amount, title, date;
        String description;
        MoneyInputObject object;

        public ViewHolder(final View itemView, final OnMoneyObjectLongClick longClick) {
            super(itemView);

            amount = itemView.findViewById(R.id.list_item_amount);
            title = itemView.findViewById(R.id.list_item_title);
            date = itemView.findViewById(R.id.list_item_date);
            final SimpleDateFormat sdf = new SimpleDateFormat("EEEE HH:mm");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                    builder.setTitle(title.getText().toString());
                    builder.setMessage(description + " vas je izasla : " + amount.getText().toString() + " na datum: \n" + sdf.format(object.getInputDate()));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClick.onMoneyObjectLongClick(object);
                    return false;
                }
            });
        }
    }

    public interface OnMoneyObjectLongClick {
        void onMoneyObjectLongClick(MoneyInputObject moneyInputObject);
    }
}
