package com.example.alen.moneymonitor;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtil {

    SharedPreferences preferences;

    public SharedPreferencesUtil(Context context) {
        preferences = context.getSharedPreferences("com.example.alen.moneymonitor", Context.MODE_PRIVATE);
    }



    public void setAmount(float amount) {
        preferences.edit().putFloat("amount", amount).apply();
    }
    public float getAmount() {
        return preferences.getFloat("amount", (float) 0);
    }
}
