package com.example.alen.moneymonitor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alen.moneymonitor.R;
import com.example.alen.moneymonitor.models.CalendarMonthsObject;
import com.example.alen.moneymonitor.models.MoneyInputObject;

import java.util.ArrayList;
import java.util.List;

public class MonthRecyclerAdapter extends RecyclerView.Adapter<MonthRecyclerAdapter.ViewHolder>{

    private List<CalendarMonthsObject> list = new ArrayList<>();
    private OnMonthClick clickListener;

    public MonthRecyclerAdapter(List<CalendarMonthsObject> list, OnMonthClick clickListener) {
        this.list = list;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.month_list_item, parent, false);
        return new MonthRecyclerAdapter.ViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String month = list.get(position).getMonthName();
        holder.calendarMonthsObject = list.get(position);
        holder.monthName.setText(month);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView monthName;
        CalendarMonthsObject calendarMonthsObject;

        public ViewHolder(View itemView, final OnMonthClick onMonthClick) {
            super(itemView);

            monthName = itemView.findViewById(R.id.month_name_list_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onMonthClick.onMonthTap(calendarMonthsObject);
                }
            });
        }
    }

    public interface OnMonthClick {
        void onMonthTap(CalendarMonthsObject calendarMonthsObject);
    }
}
