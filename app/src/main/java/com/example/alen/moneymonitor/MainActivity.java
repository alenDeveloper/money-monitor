package com.example.alen.moneymonitor;

import android.animation.Animator;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alen.moneymonitor.adapters.MainRecyclerAdapter;
import com.example.alen.moneymonitor.adapters.MonthRecyclerAdapter;
import com.example.alen.moneymonitor.models.CalendarMonthsObject;
import com.example.alen.moneymonitor.models.CalendarMonthsObject_;
import com.example.alen.moneymonitor.models.MoneyInputObject;
import com.example.alen.moneymonitor.models.MyObjectBox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.QueryBuilder;

public class MainActivity extends AppCompatActivity {

    CardView newInputHolder;
    EditText moneyInput, titleInput, descriptionInput;
    CheckBox addMoneyCheckbox, subtractMoneyCheckbox;
    TextView moneyBalanceText, selectedMonthText;
    CardView pickMonthListHolder, spentProMonthHolder;
    RecyclerView monthList;
    RecyclerView mainRecyclerView;
    ImageView floatingActionBtn;
    ListView spentProMonthList;

    MainRecyclerAdapter mainRecyclerAdapter;
    ArrayList<MoneyInputObject> mainMoneyInputObjectList;
    MainRecyclerAdapter.OnMoneyObjectLongClick longClick;

    MonthRecyclerAdapter monthRecyclerAdapter;
    SharedPreferencesUtil preferencesUtil;

    private Box<CalendarMonthsObject> calendarMonthsObjects;
    BoxStore boxStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boxStore = MyObjectBox.builder().androidContext(this).build();
        calendarMonthsObjects = boxStore.boxFor(CalendarMonthsObject.class);

        initialize();
        setMoneyBalanceText();
        setMainList();
        setMainRecyclerAdapter();
        setMonths();
    }

    @Override
    protected void onPause() {
        super.onPause();
        boxStore.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (boxStore.isClosed()) {
            boxStore = MyObjectBox.builder().androidContext(this).build();
        }
    }

    void setMoneyBalanceText() {
        List<CalendarMonthsObject> monthsObjects = calendarMonthsObjects.getAll();
        float amount = 0;
        if (monthsObjects == null || monthsObjects.isEmpty()) {
            return;
        }
        for (CalendarMonthsObject object : monthsObjects) {
            amount =+ object.getMonthSpent();
        }
        //float currentAmount = preferencesUtil.getAmount();

        setMoneyBalanceText(amount);
    }

    void setMoneyBalanceText(float amount) {
        String value = String.valueOf(amount) + " $";
        moneyBalanceText.setText(value);
    }

/*    void addToMoneyBalance(float amount) {
        float currentAmount = preferencesUtil.getAmount();
        currentAmount += amount;
        preferencesUtil.setAmount(currentAmount);

        setMoneyBalanceText(currentAmount);
    }

    void subtractMoneyBalance(float amount) {
        float currentAmount = preferencesUtil.getAmount();
        currentAmount -= amount;
        preferencesUtil.setAmount(currentAmount);

        setMoneyBalanceText(currentAmount);
    }*/

    void setMainRecyclerAdapter() {
        longClick = new MainRecyclerAdapter.OnMoneyObjectLongClick() {
            @Override
            public void onMoneyObjectLongClick(MoneyInputObject moneyInputObject) {
                float inputAmount = moneyInputObject.isAdding() ? -moneyInputObject.getAmount() : moneyInputObject.getAmount();

                //addToMoneyBalance(inputAmount);
                QueryBuilder<CalendarMonthsObject> builder = calendarMonthsObjects.query();
                builder.equal(CalendarMonthsObject_.monthNumber, moneyInputObject.getInputDate().getMonth());

                CalendarMonthsObject calendarMonthsObject = builder.build().findFirst();

                float currentMonthAmount = calendarMonthsObject.getMonthSpent();
                currentMonthAmount += inputAmount;

                boxStore.boxFor(MoneyInputObject.class).remove(moneyInputObject);
                calendarMonthsObject.setMonthSpent(currentMonthAmount);
                boxStore.boxFor(CalendarMonthsObject.class).put(calendarMonthsObject);


                mainRecyclerAdapter.notifyItemRangeRemoved(mainMoneyInputObjectList.indexOf(moneyInputObject), mainMoneyInputObjectList.size());
                mainMoneyInputObjectList.remove(moneyInputObject);
                setMoneyBalanceText();
            }
        };
        mainRecyclerAdapter = new MainRecyclerAdapter(mainMoneyInputObjectList, longClick);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);

        mainRecyclerView.setLayoutManager(mLayoutManager);
        mainRecyclerView.setAdapter(mainRecyclerAdapter);
    }

    void setMonths() {
        List<CalendarMonthsObject> monthsObjects = calendarMonthsObjects.getAll();

        MonthRecyclerAdapter.OnMonthClick onMonthClick = new MonthRecyclerAdapter.OnMonthClick() {
            @Override
            public void onMonthTap(CalendarMonthsObject calendarMonthsObject) {
                CalendarMonthsObject month = boxStore.boxFor(CalendarMonthsObject.class).get(calendarMonthsObject.getId());
                mainMoneyInputObjectList.clear();

                mainMoneyInputObjectList.addAll(month.moneyInputObjects);

                mainRecyclerAdapter = new MainRecyclerAdapter(mainMoneyInputObjectList, longClick);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);

                mainRecyclerView.setLayoutManager(mLayoutManager);
                mainRecyclerView.setAdapter(mainRecyclerAdapter);
                mainRecyclerAdapter.notifyDataSetChanged();

                pickMonthListHolder.setVisibility(View.GONE);
                selectedMonthText.setText(calendarMonthsObject.getMonthName());

            }
        };

        monthRecyclerAdapter = new MonthRecyclerAdapter(monthsObjects, onMonthClick);
        monthList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        monthList.setAdapter(monthRecyclerAdapter);
    }

    void setMainList() {
        ArrayList<MoneyInputObject> list = new ArrayList<>();

        Date today = new Date();
        int currentMonth = today.getMonth();

        List<CalendarMonthsObject> calendarMonthsObjects = boxStore.boxFor(CalendarMonthsObject.class).getAll();

        for (CalendarMonthsObject month : calendarMonthsObjects) {
            int calendarMonth = month.getMonthNumber();
            if (calendarMonth == currentMonth) {
                list.addAll(month.moneyInputObjects);
            }
        }

        mainMoneyInputObjectList = list;
    }

    public void selectMonth(View view) {
        if (pickMonthListHolder.getVisibility() == View.VISIBLE) {
            pickMonthListHolder.setVisibility(View.GONE);
        } else {
            pickMonthListHolder.setVisibility(View.VISIBLE);
        }
    }

    public void addInput(View view) {
        if (createMoneyObject()) {
            doAnimation(floatingActionBtn, newInputHolder);
        } else {
            Toast.makeText(getApplicationContext(), "You have to set amount and title fields", Toast.LENGTH_SHORT).show();
        }
        setMoneyBalanceText();
    }

    public boolean createMoneyObject() {
        MoneyInputObject moneyInputObject = new MoneyInputObject();

        String title = titleInput.getText().toString();
        String description = descriptionInput.getText().toString();
        Float amount;
        try {
            amount = Float.valueOf(moneyInput.getText().toString());
        } catch (Exception ex) {
            amount = (float) 0;
        }
        Date date = new Date();

        if (!title.isEmpty()) {
            moneyInputObject.setTitle(title);
        } else {
            return false;
        }

        if (!description.isEmpty()) {
            moneyInputObject.setDescription(description);
        } else {
            moneyInputObject.setDescription("");
        }

        if (amount != 0) {
            moneyInputObject.setAmount(amount);
        } else {
            return false;
        }

        moneyInputObject.setInputDate(date);

        if (addMoneyCheckbox.isChecked()) {
            moneyInputObject.setAdding(true);
            //addToMoneyBalance(amount);
        } else {
            moneyInputObject.setAdding(false);
            //subtractMoneyBalance(amount);
        }

        connectMoneyObjectToCalendarObject(moneyInputObject);
        setMainList();
        mainRecyclerAdapter.notifyDataSetChanged();

        clearInputFields();
        setMonths();
        return true;
    }

    void connectMoneyObjectToCalendarObject(MoneyInputObject object) {
        int objectMonth = object.getInputDate().getMonth();
        float monthAmount = object.isAdding() ? object.getAmount() : -object.getAmount();

        List<CalendarMonthsObject> monthsObjects = calendarMonthsObjects.getAll();

        for (CalendarMonthsObject calendarMonthsObject : monthsObjects) {
            int calendarObjectMonthNumber = calendarMonthsObject.getMonthNumber();

            if (objectMonth == calendarObjectMonthNumber) {
                float monthSpent = calendarMonthsObject.getMonthSpent();
                monthSpent += monthAmount;
                calendarMonthsObject.setMonthSpent(monthSpent);
                mainMoneyInputObjectList.add(object);
                calendarMonthsObject.moneyInputObjects.add(object);
                boxStore.boxFor(CalendarMonthsObject.class).put(calendarMonthsObject);
                return;
            }
        }

        CalendarMonthsObject calendarMonthsObject = new CalendarMonthsObject();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        String monthName = sdf.format(object.getInputDate());

        calendarMonthsObject.moneyInputObjects.add(object);
        calendarMonthsObject.setMonthName(monthName);
        calendarMonthsObject.setMonthNumber(object.getInputDate().getMonth());
        calendarMonthsObject.setMonthSpent(monthAmount);

        boxStore.boxFor(CalendarMonthsObject.class).put(calendarMonthsObject);
    }

    public void clearInputFields() {
        descriptionInput.setText("");
        titleInput.setText("");
        moneyInput.setText("");
    }

    public void cancelInput(View view) {
        doAnimation(floatingActionBtn, newInputHolder);
    }

    public void addNewInputFloatingActionBtn(View view) {
        doAnimation(newInputHolder, floatingActionBtn);
    }

    public void initialize() {
        preferencesUtil = new SharedPreferencesUtil(getApplicationContext());
        newInputHolder = findViewById(R.id.add_new_input_holder);
        moneyInput = findViewById(R.id.money_input);
        titleInput = findViewById(R.id.title_input);
        descriptionInput = findViewById(R.id.description_input);
        addMoneyCheckbox = findViewById(R.id.add_money_check);
        subtractMoneyCheckbox = findViewById(R.id.subtract_money_check);
        moneyBalanceText = findViewById(R.id.money_balance_text);
        selectedMonthText = findViewById(R.id.selected_month_text);
        pickMonthListHolder = findViewById(R.id.select_month_holder);
        monthList = findViewById(R.id.months_list);
        mainRecyclerView = findViewById(R.id.main_recycler_list);
        floatingActionBtn = findViewById(R.id.floating_action_btn);
        spentProMonthHolder = findViewById(R.id.spent_pro_month_holder);
        spentProMonthList = findViewById(R.id.spent_pro_month_list);

    }

    public void doAnimation(final View opening, final View closing) {
        opening.setScaleX(0);
        opening.setScaleY(0);
        opening.animate().scaleX(1).scaleY(1).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                opening.setVisibility(View.VISIBLE);
                closing.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }

    public void checkAdd(View view) {
        if (subtractMoneyCheckbox.isChecked()) {
            subtractMoneyCheckbox.setChecked(false);
        }
    }

    public void checkSubtract(View view) {
        if (addMoneyCheckbox.isChecked()) {
            addMoneyCheckbox.setChecked(false);
        }
    }

    public void showSpentProMonth(View view) {
        if (spentProMonthHolder.getVisibility() == View.GONE) {
            List<CalendarMonthsObject> calendarMonthsObjects = boxStore.boxFor(CalendarMonthsObject.class).getAll();

            List<String> proMonthSpentList = new ArrayList<>();

            for (CalendarMonthsObject object : calendarMonthsObjects) {
                float spentAmount = 0;
                for (MoneyInputObject moneyInputObject : object.moneyInputObjects) {
                    if (moneyInputObject != null && !moneyInputObject.isAdding()) {
                        spentAmount += moneyInputObject.getAmount();
                    }
                }
                String data = object.getMonthName() + " " + String.valueOf(object.getMonthSpent()) + " (-" + String.valueOf(spentAmount) + ")";
                proMonthSpentList.add(data);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, proMonthSpentList);
            spentProMonthList.setAdapter(adapter);
            spentProMonthHolder.setVisibility(View.VISIBLE);
        } else {
            spentProMonthHolder.setVisibility(View.GONE);
        }

    }
}
