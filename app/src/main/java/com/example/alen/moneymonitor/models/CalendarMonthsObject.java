package com.example.alen.moneymonitor.models;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

@Entity
public class CalendarMonthsObject {

    @Id
    public long id;

    public String monthName;
    public int monthNumber;
    public float monthSpent;

    @Backlink
    public ToMany<MoneyInputObject> moneyInputObjects;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public float getMonthSpent() {
        return monthSpent;
    }

    public void setMonthSpent(float monthSpent) {
        this.monthSpent = monthSpent;
    }
}
